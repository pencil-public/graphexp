FROM nginx:1.13.9

ENV http_proxy $http_proxy
ENV https_proxy $https_proxy
ENV no_proxy $no_proxy
ENV PATH /usr/local/bin:$PATH

ADD . /usr/share/nginx/html/
ADD nginx.conf /etc/nginx/

WORKDIR  /usr/share/nginx/html